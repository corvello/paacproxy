<?php
if(basename($_SERVER['SCRIPT_FILENAME'])==basename(__FILE__))
	exit;

/**
 * PersonalAlertService class
 * 
 *  
 * 
 * @author    {Milton Corvello}
 * @copyright {Plancorp}
 * @package   {PersonalAlertService}
 */
class PersonalAlertService {

  /**
   *  
   *
   * @param createService $parameters
   * @return createServiceResponse
   */
  public function createService(createService $parameters) {
    
    $createServiceResponse = new $createServiceResponse();
    
    return $parameters;
  }

  /**
   *  
   *
   * @param messageWebReturn $parameters
   * @return messageWebReturnResponse
   */
  public function messageWebReturn(messageWebReturn $parameters) {
    return $this->__soapCall('messageWebReturn', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param messageWebResponse $parameters
   * @return messageWebResponseResponse
   */
  public function messageWebResponse(messageWebResponse $parameters) {
    return $this->__soapCall('messageWebResponse', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param runExecuted $parameters
   * @return runExecutedResponse
   */
  public function runExecuted(runExecuted $parameters) {
    return $this->__soapCall('runExecuted', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param runScheduled $parameters
   * @return runScheduledResponse
   */
  public function runScheduled(runScheduled $parameters) {
    return $this->__soapCall('runScheduled', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getAllStepIntanceByAlertInstanceCode $parameters
   * @return getAllStepIntanceByAlertInstanceCodeResponse
   */
  public function getAllStepIntanceByAlertInstanceCode(getAllStepIntanceByAlertInstanceCode $parameters) {
    return $this->__soapCall('getAllStepIntanceByAlertInstanceCode', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getPersonalAlert $parameters
   * @return getPersonalAlertResponse
   */
  public function getPersonalAlert(getPersonalAlert $parameters) {
    return $this->__soapCall('getPersonalAlert', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param createPersonalAlert $parameters
   * @return createPersonalAlertResponse
   */
  public function createPersonalAlert(createPersonalAlert $parameters) {
    return $this->__soapCall('createPersonalAlert', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param cancelAlertInstance $parameters
   * @return cancelAlertInstanceResponse
   */
  public function cancelAlertInstance(cancelAlertInstance $parameters) {
    return $this->__soapCall('cancelAlertInstance', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param runFlow $parameters
   * @return runFlowResponse
   */
  public function runFlow(runFlow $parameters) {
    return $this->__soapCall('runFlow', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getRSVPByAlertInstanceCode $parameters
   * @return getRSVPByAlertInstanceCodeResponse
   */
  public function getRSVPByAlertInstanceCode(getRSVPByAlertInstanceCode $parameters) {
    return $this->__soapCall('getRSVPByAlertInstanceCode', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param newScheduling $parameters
   * @return newSchedulingResponse
   */
  public function newScheduling(newScheduling $parameters) {
    return $this->__soapCall('newScheduling', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param schedulePersonalAlertByReceptorGroup $parameters
   * @return schedulePersonalAlertByReceptorGroupResponse
   */
  public function schedulePersonalAlertByReceptorGroup(schedulePersonalAlertByReceptorGroup $parameters) {
    return $this->__soapCall('schedulePersonalAlertByReceptorGroup', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param editScheduling $parameters
   * @return editSchedulingResponse
   */
  public function editScheduling(editScheduling $parameters) {
    return $this->__soapCall('editScheduling', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param removeScheduling $parameters
   * @return removeSchedulingResponse
   */
  public function removeScheduling(removeScheduling $parameters) {
    return $this->__soapCall('removeScheduling', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param generateRunCode $parameters
   * @return generateRunCodeResponse
   */
  public function generateRunCode(generateRunCode $parameters) {
    return $this->__soapCall('generateRunCode', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param cancelAlertInstanceByRunCode $parameters
   * @return cancelAlertInstanceByRunCodeResponse
   */
  public function cancelAlertInstanceByRunCode(cancelAlertInstanceByRunCode $parameters) {
    return $this->__soapCall('cancelAlertInstanceByRunCode', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param cancelSchedullingByRunCode $parameters
   * @return cancelSchedullingByRunCodeResponse
   */
  public function cancelSchedullingByRunCode(cancelSchedullingByRunCode $parameters) {
    return $this->__soapCall('cancelSchedullingByRunCode', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param setTimerFileUpload $parameters
   * @return setTimerFileUploadResponse
   */
  public function setTimerFileUpload(setTimerFileUpload $parameters) {
    return $this->__soapCall('setTimerFileUpload', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param countUploadedFiles $parameters
   * @return countUploadedFilesResponse
   */
  public function countUploadedFiles(countUploadedFiles $parameters) {
    return $this->__soapCall('countUploadedFiles', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param findUploadedFiles $parameters
   * @return findUploadedFilesResponse
   */
  public function findUploadedFiles(findUploadedFiles $parameters) {
    return $this->__soapCall('findUploadedFiles', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param findUploadedFile $parameters
   * @return findUploadedFileResponse
   */
  public function findUploadedFile(findUploadedFile $parameters) {
    return $this->__soapCall('findUploadedFile', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param updateUploadedFile $parameters
   * @return updateUploadedFileResponse
   */
  public function updateUploadedFile(updateUploadedFile $parameters) {
    return $this->__soapCall('updateUploadedFile', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param discartUploadedFile $parameters
   * @return discartUploadedFileResponse
   */
  public function discartUploadedFile(discartUploadedFile $parameters) {
    return $this->__soapCall('discartUploadedFile', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param importUploadedFile $parameters
   * @return importUploadedFileResponse
   */
  public function importUploadedFile(importUploadedFile $parameters) {
    return $this->__soapCall('importUploadedFile', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param countBatches $parameters
   * @return countBatchesResponse
   */
  public function countBatches(countBatches $parameters) {
    error_log($parameters->initialDate." ".$parameters->finalDate, 1, "luiz.guimaraes@plancorp.net");
    $countBatchesResponse = 1000;
    return $countBatchesResponse;
  }

  /**
   *  
   *
   * @param getReceptorGroupByName $parameters
   * @return getReceptorGroupByNameResponse
   */
  public function getReceptorGroupByName(getReceptorGroupByName $parameters) {
    return $this->__soapCall('getReceptorGroupByName', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param registerReceptor $parameters
   * @return registerReceptorResponse
   */
  public function registerReceptor(registerReceptor $parameters) {
    return $this->__soapCall('registerReceptor', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param createPersonalAlertPlus $parameters
   * @return createPersonalAlertPlusResponse
   */
  public function createPersonalAlertPlus(createPersonalAlertPlus $parameters) {
    return $this->__soapCall('createPersonalAlertPlus', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param removePersonalAlert $parameters
   * @return removePersonalAlertResponse
   */
  public function removePersonalAlert(removePersonalAlert $parameters) {
    return $this->__soapCall('removePersonalAlert', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param findBatches $parameters
   * @return findBatchesResponse
   */
  public function findBatches(findBatches $parameters) {
    return $this->__soapCall('findBatches', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param findBatch $parameters
   * @return findBatchResponse
   */
  public function findBatch(findBatch $parameters) {
    return $this->__soapCall('findBatch', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param updateBatch $parameters
   * @return updateBatchResponse
   */
  public function updateBatch(updateBatch $parameters) {
    return $this->__soapCall('updateBatch', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param createBatch $parameters
   * @return createBatchResponse
   */
  public function createBatch(createBatch $parameters) {
    return $this->__soapCall('createBatch', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param processBatch $parameters
   * @return processBatchResponse
   */
  public function processBatch(processBatch $parameters) {
    return $this->__soapCall('processBatch', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getReceptorGroupById $parameters
   * @return getReceptorGroupByIdResponse
   */
  public function getReceptorGroupById(getReceptorGroupById $parameters) {
    return $this->__soapCall('getReceptorGroupById', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param findServiceById $parameters
   * @return findServiceByIdResponse
   */
  public function findServiceById(findServiceById $parameters) {
    return $this->__soapCall('findServiceById', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param discartBatch $parameters
   * @return discartBatchResponse
   */
  public function discartBatch(discartBatch $parameters) {
    return $this->__soapCall('discartBatch', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param findAllServices $parameters
   * @return findAllServicesResponse
   */
  public function findAllServices(findAllServices $parameters) {
    return 1;
  }

  /**
   *  
   *
   * @param findRangeOfServices $parameters
   * @return findRangeOfServicesResponse
   */
  public function findRangeOfServices(findRangeOfServices $parameters) {
    return $this->__soapCall('findRangeOfServices', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param editService $parameters
   * @return editServiceResponse
   */
  public function editService(editService $parameters) {
    return $this->__soapCall('editService', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param editReceptorGroup $parameters
   * @return editReceptorGroupResponse
   */
  public function editReceptorGroup(editReceptorGroup $parameters) {
    return $this->__soapCall('editReceptorGroup', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param findServiceByName $parameters
   * @return findServiceByNameResponse
   */
  public function findServiceByName(findServiceByName $parameters) {
    return $this->__soapCall('findServiceByName', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param newReceptorGroup $parameters
   * @return newReceptorGroupResponse
   */
  public function newReceptorGroup(newReceptorGroup $parameters) {
    return $this->__soapCall('newReceptorGroup', array($parameters),       array(
            'uri' => 'http://plancorp.net/paac/papr/personal',
            'soapaction' => ''
           )
      );
  }

}

?>
