<?php

if(basename($_SERVER['SCRIPT_FILENAME'])==basename(__FILE__))
	exit;

// The @pw_element and @pw_complex are non-standard keywords for documentaion 
// I had to define to support those complex types for WSDL generation. The  
// "pw" stands for "PhpWsdl". But who cares?

/**
 * @pw_element string $name A string with a value
 * @pw_complex findServiceByName The complex type name definition
 */
class findServiceByName {
  public $name; // string
}
/**
 * @pw_element serviceEntity $return A serviceEntity with a value
 * @pw_complex findServiceByNameResponse The complex type name definition
 */
class findServiceByNameResponse {
  public $return; // serviceEntity
}
/**
  * @pw_element  string $callBackPassword; // string
  * @pw_element  string $callBackUrl; // string
  * @pw_element  string $callBackUser; // string
  * @pw_element  boolean $delayOfOneDayInTheFirstStep; // boolean
  * @pw_element  string $description; // string
  * @pw_element  int $id; // int
  * @pw_element  string $name; // string
  * @pw_element  string $origin; // string
  * @pw_element  long $version; // long
  * @pw_element  string $workdays; // string
  * @pw_element  string $workingHours; // string
  * @pw_complex serviceEntity The complex type name definition
  */
class serviceEntity {
  public $callBackPassword; // string
  public $callBackUrl; // string
  public $callBackUser; // string
  public $delayOfOneDayInTheFirstStep; // boolean
  public $description; // string
  public $id; // int
  public $name; // string
  public $origin; // string
  public $version; // long
  public $workdays; // string
  public $workingHours; // string
}
/**
  * @pw_element  string $detail; // string
  * @pw_element  string $faultcode; // string
  * @pw_element  string $faultstring; // string
  * @pw_complex paprExceptionDetail The complex type name definition
  */
class paprExceptionDetail {
  public $detail; // string
  public $faultcode; // string
  public $faultstring; // string
}
/**
 * @pw_element string $name; // string 
 * @pw_complex getReceptorGroupByName The complex type name definition
 */
class getReceptorGroupByName {
  public $name; // string
}
/**
 * @pw_element receptorGroupEntity $return; // receptorGroupEntity
 * @pw_complex getReceptorGroupByNameResponse The complex type name definition
 */
class getReceptorGroupByNameResponse {
  public $return; // receptorGroupEntity
}
/**
 * @pw_element int $id; // int
 * @pw_element string $receptorGroupName; // string
 * @pw_element long $version; // long
 * @pw_complex receptorGroupEntity The complex type name definition
 */
class receptorGroupEntity {
  public $id; // int
  public $receptorGroupName; // string
  public $version; // long
}
/**
 * @pw_element string $alertInstanceCode; // string
 * @pw_complex getAllStepIntanceByAlertInstanceCode The complex type name definition
 */
class getAllStepIntanceByAlertInstanceCode {
  public $alertInstanceCode; // string
}
/**
 * @pw_element stepInstanceEntity $return; // stepInstanceEntity
 * @pw_complex getAllStepIntanceByAlertInstanceCodeResponse The complex type name definition
 */
class getAllStepIntanceByAlertInstanceCodeResponse {
  public $return; // stepInstanceEntity
}
/**
 * @pw_element alertInstanceEntity $alertInstance; // alertInstanceEntity
 * @pw_element boolean $calledBack; // boolean 
 * @pw_element dateTime $createDate; // dateTime 
 * @pw_element int $createUser; // int
 * @pw_element int $id; // int
 * @pw_element int $idAlertStep; // int 
 * @pw_element int 
 * @pw_element string $messageContent; // string
 * @pw_element string $messageSubject; // string
 * @pw_element string $messageType; // string
 * @pw_element int $numberOfRepetition; // int 
 * @pw_element string $stepContent; // string
 * @pw_element string $stepMessageCode; // string
 * @pw_element string $stepMessageStatus; // string
 * @pw_element string $stepReceptor; // string
 * @pw_element string $stepReceptorAddress; // string
 * @pw_element dateTime $stepReplyDatetime; // dateTime
 * @pw_element int $stepReplyStatus; // int
 * @pw_element dateTime $stepSendDatetime; // dateTime
 * @pw_element long $version; // long
 * @pw_complex stepInstanceEntity The complex type name definition
 */
class stepInstanceEntity {
  public $alertInstance; // alertInstanceEntity
  public $calledBack; // boolean
  public $createDate; // dateTime
  public $createUser; // int
  public $id; // int
  public $idAlertStep; // int
  public $messageContent; // string
  public $messageSubject; // string
  public $messageType; // string
  public $numberOfRepetition; // int
  public $stepContent; // string
  public $stepMessageCode; // string
  public $stepMessageStatus; // string
  public $stepReceptor; // string
  public $stepReceptorAddress; // string
  public $stepReplyDatetime; // dateTime
  public $stepReplyStatus; // int
  public $stepSendDatetime; // dateTime
  public $version; // long
}
/**
 * @pw_element int $alertIdalert; // int
 * @pw_element string $alertInstanceCode; // string
 * @pw_element string $comment; // string
 * @pw_element dateTime $createDate; // dateTime
 * @pw_element long $createUser; // long
 * @pw_element dateTime $emissionDateTime; // dateTime
 * @pw_element int $id; // int
 * @pw_element string $messageContent; // string
 * @pw_element string $messageSubject; // string
 * @pw_element string $messageType; // string
 * @pw_element string $rsvp; // string
 * @pw_element string $runCode; // string
 * @pw_element string $serviceDescription; // string
 * @pw_element string $serviceName; // string
 * @pw_element string $serviceOrigin; // string
 * @pw_element int $status; // int
 * @pw_element long $version; // long
 * @pw_complex alertInstanceEntity The complex type name definition
 */
class alertInstanceEntity {
  public $alertIdalert; // int
  public $alertInstanceCode; // string
  public $comment; // string
  public $createDate; // dateTime
  public $createUser; // long
  public $emissionDateTime; // dateTime
  public $id; // int
  public $messageContent; // string
  public $messageSubject; // string
  public $messageType; // string
  public $rsvp; // string
  public $runCode; // string
  public $serviceDescription; // string
  public $serviceName; // string
  public $serviceOrigin; // string
  public $status; // int
  public $version; // long
}
/**
 * @pw_element string $runCode; // string
 * @pw_complex cancelSchedullingByRunCode The complex type name definition
 */
class cancelSchedullingByRunCode {
  public $runCode; // string
}
/**
 * @pw_element int $return; // int
 * @pw_complex cancelSchedullingByRunCodeResponse The complex type name definition
 */
class cancelSchedullingByRunCodeResponse {
  public $return; // int
}
/**
 * @pw_element string $receptorGroupName; // string
 * @pw_complex newReceptorGroup The complex type name definition
 */
class newReceptorGroup {
  public $receptorGroupName; // string
}
/**
 * @pw_element int $return; // int
 * @pw_complex newReceptorGroupResponse The complex type name definition
 */
class newReceptorGroupResponse {
  public $return; // int
}
/**
 * @pw_element int $id; // int
 * @pw_element string $nameNew; // string
 * @pw_element string $descriptionNew; // string
 * @pw_element string $originNew; // string
 * @pw_element string $workdaysNew; // string
 * @pw_element string $workingHoursNew; // string
 * @pw_element boolean $delayOfOneDayInTheFirstStepNew; // boolean
 * @pw_element string $callBackUrlNew; // string
 * @pw_element string $callBackUserNew; // string
 * @pw_element string $callBackPasswordNew; // string
 * @pw_complex editService The complex type name definition
 */
class editService {
  public $id; // int
  public $nameNew; // string
  public $descriptionNew; // string
  public $originNew; // string
  public $workdaysNew; // string
  public $workingHoursNew; // string
  public $delayOfOneDayInTheFirstStepNew; // boolean
  public $callBackUrlNew; // string
  public $callBackUserNew; // string
  public $callBackPasswordNew; // string
}
/**
 * @pw_element serviceEntity $return; // serviceEntity
 * @pw_complex editServiceResponse The complex type name definition
 */
class editServiceResponse {
  public $return; // serviceEntity
}
/**
 * @pw_element int $id; // int
 * @pw_element string $subject; // string
 * @pw_element string $description; // string
 * @pw_element string $schedulingDateTime; // string
 * @pw_element int $filterId; // int
 * @pw_element int $serviceId; // int
 * @pw_element string $sqlQuery; // string
 * @pw_complex updateBatch The complex type name definition
 */
class updateBatch {
  public $id; // int
  public $subject; // string
  public $description; // string
  public $schedulingDateTime; // string
  public $filterId; // int
  public $serviceId; // int
  public $sqlQuery; // string
}
/**
 * @pw_element batchPojo $return; // batchPojo
 * @pw_complex updateBatchResponse The complex type name definition
 */
class updateBatchResponse {
  public $return; // batchPojo
}
/**
 * @pw_element dateTime $createDate; // dateTime
 * @pw_element boolean $createPersonalAlert; // boolean
 * @pw_element dateTime $createPersonalAlertDateEnded; // dateTime
 * @pw_element dateTime $createPersonalAlertDateStarted; // dateTime
 * @pw_element long $createPersonalAlertElapsedTime; // long
 * @pw_element int $createPersonalAlertRecordsProcessed; // int
 * @pw_element string $createUser; // string
 * @pw_element string $description; // string
 * @pw_element int $filterId; // int
 * @pw_element int $id; // int
 * @pw_element dateTime $lastUpdateDate; // dateTime
 * @pw_element string $lastUpdateUser; // string
 * @pw_element boolean $linkService; // boolean
 * @pw_element dateTime $linkServiceDateEnded; // dateTime
 * @pw_element dateTime $linkServiceDateStarted; // dateTime
 * @pw_element long $linkServiceElapsedTime; // long
 * @pw_element int $linkServiceRecordsProcessed; // int
 * @pw_element boolean $registerReceptor; // boolean
 * @pw_element dateTime $registerReceptorDateEnded; // dateTime
 * @pw_element dateTime $registerReceptorDateStarted; // dateTime
 * @pw_element long $registerReceptorElapsedTime; // long
 * @pw_element int $registerReceptorRecordsProcessed; // int
 * @pw_element boolean $run; // boolean
 * @pw_element dateTime $runDateEnded; // dateTime
 * @pw_element dateTime $runDateStarted; // dateTime
 * @pw_element long $runElapsedTime; // long
 * @pw_element int $runRecordsProcessed; // int
 * @pw_element string $runType; // string
 * @pw_element string $runcode; // string
 * @pw_element dateTime $schedulingDateTime; // dateTime
 * @pw_element int $serviceId; // int
 * @pw_element string $sqlQuery; // string
 * @pw_element string $status; // string
 * @pw_element string $subject; // string
 * @pw_element int $totalRecords; // int
 * @pw_complex batchPojo The complex type name definition
 */
class batchPojo {
  public $createDate; // dateTime
  public $createPersonalAlert; // boolean
  public $createPersonalAlertDateEnded; // dateTime
  public $createPersonalAlertDateStarted; // dateTime
  public $createPersonalAlertElapsedTime; // long
  public $createPersonalAlertRecordsProcessed; // int
  public $createUser; // string
  public $description; // string
  public $filterId; // int
  public $id; // int
  public $lastUpdateDate; // dateTime
  public $lastUpdateUser; // string
  public $linkService; // boolean
  public $linkServiceDateEnded; // dateTime
  public $linkServiceDateStarted; // dateTime
  public $linkServiceElapsedTime; // long
  public $linkServiceRecordsProcessed; // int
  public $registerReceptor; // boolean
  public $registerReceptorDateEnded; // dateTime
  public $registerReceptorDateStarted; // dateTime
  public $registerReceptorElapsedTime; // long
  public $registerReceptorRecordsProcessed; // int
  public $run; // boolean
  public $runDateEnded; // dateTime
  public $runDateStarted; // dateTime
  public $runElapsedTime; // long
  public $runRecordsProcessed; // int
  public $runType; // string
  public $runcode; // string
  public $schedulingDateTime; // dateTime
  public $serviceId; // int
  public $sqlQuery; // string
  public $status; // string
  public $subject; // string
  public $totalRecords; // int
}
/**
 * @pw_element int $posInitial; // int
 * @pw_element int $posFinal; // int
 * @pw_element string $initialDate; // string
 * @pw_element string $finalDate; // string
 * @pw_complex findBatches The complex type name definition
 */
class findBatches {
  public $posInitial; // int
  public $posFinal; // int
  public $initialDate; // string
  public $finalDate; // string
}
/**
 * @pw_element batchPojo $return; // batchPojo
 * @pw_complex findBatchesResponse The complex type name definition
 */
class findBatchesResponse {
  public $return; // batchPojo
}
/**
 * @pw_complex findAllServices The complex type name definition
 */
class findAllServices {
}
/**
 * @pw_element serviceEntity $return; // serviceEntity
 * @pw_complex findAllServicesResponse The complex type name definition
 */
class findAllServicesResponse {
  public $return; // serviceEntity
}
/**
 * @pw_element int $posInitial; // int
 * @pw_element int $posFinal; // int
 * @pw_complex findRangeOfServices The complex type name definition
 */
class findRangeOfServices {
  public $posInitial; // int
  public $posFinal; // int
}
/**
 * @pw_element serviceEntity $return; // serviceEntity
 * @pw_complex findRangeOfServicesResponse The complex type name definition
 */
class findRangeOfServicesResponse {
  public $return; // serviceEntity
}
/**
 * @pw_element int $id; // int
 * @pw_complex findBatch The complex type name definition
 */
class findBatch {
  public $id; // int
}
/**
 * @pw_element batchPojo $return; // batchPojo
 * @pw_complex findBatchResponse The complex type name definition
 */
class findBatchResponse {
  public $return; // batchPojo
}
/**
 * @pw_element string $username; // string
 * @pw_element string $password; // string
 * @pw_element string $channel; // string
 * @pw_element string $address; // string
 * @pw_element string $timeToWait; // string
 * @pw_element int $alertContent; // int
 * @pw_element int $maxRepetition; // int
 * @pw_element string $responseRegex; // string
 * @pw_element string $messageType; // string
 * @pw_element string $messageSubject; // string
 * @pw_element string $messageContent; // string
 * @pw_element int $idService; // int
 * @pw_element boolean $interactive; // boolean
 * @pw_complex createPersonalAlert The complex type name definition
 */
class createPersonalAlert {
  public $username; // string
  public $password; // string
  public $channel; // string
  public $address; // string
  public $timeToWait; // string
  public $alertContent; // int
  public $maxRepetition; // int
  public $responseRegex; // string
  public $messageType; // string
  public $messageSubject; // string
  public $messageContent; // string
  public $idService; // int
  public $interactive; // boolean
}
/**
 * @pw_element string $return; // string
 * @pw_complex createPersonalAlertResponse The complex type name definition
 */
class createPersonalAlertResponse {
  public $return; // string
}
/**
 * @pw_element string $alertInstanceCode; // string
 * @pw_complex removeScheduling The complex type name definition
 */
class removeScheduling {
  public $alertInstanceCode; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex removeSchedulingResponse The complex type name definition
 */
class removeSchedulingResponse {
  public $return; // string
}
/**
 * @pw_element string $username; // string
 * @pw_element string $password; // string
 * @pw_complex removePersonalAlert The complex type name definition
 */
class removePersonalAlert {
  public $username; // string
  public $password; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex removePersonalAlertResponse The complex type name definition
 */
class removePersonalAlertResponse {
  public $return; // string
}
/**
 * @pw_element string $alertInstanceCode; // string
 * @pw_element string $schedulingDateTime; // string
 * @pw_element string $messageType; // string
 * @pw_element string $messageSubject; // string
 * @pw_element string $messageContent; // string
 * @pw_element int $idAlert; // int
 * @pw_element replacement $replacements; // replacement
 * @pw_complex editScheduling The complex type name definition
 */
class editScheduling {
  public $alertInstanceCode; // string
  public $schedulingDateTime; // string
  public $messageType; // string
  public $messageSubject; // string
  public $messageContent; // string
  public $idAlert; // int
  public $replacements; // replacement
}
/**
 * @pw_element string $key; // string
 * @pw_element string $value; // string
 * @pw_complex replacement The complex type name definition
 */
class replacement {
  public $key; // string
  public $value; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex editSchedulingResponse The complex type name definition
 */
class editSchedulingResponse {
  public $return; // string
}
/**
 * @pw_element string $runDescription; // string
 * @pw_complex generateRunCode The complex type name definition
 */
class generateRunCode {
  public $runDescription; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex generateRunCodeResponse The complex type name definition
 */
class generateRunCodeResponse {
  public $return; // string
}
/**
 * @pw_element int $id; // int
 * @pw_complex discartBatch The complex type name definition
 */
class discartBatch {
  public $id; // int
}
/**
 * @pw_element boolean $return; // boolean
 * @pw_complex discartBatchResponse The complex type name definition
 */
class discartBatchResponse {
  public $return; // boolean
}
/**
 * @pw_element string $runCode; // string
 * @pw_complex runScheduled The complex type name definition
 */
class runScheduled {
  public $runCode; // string
}
/**
 * @pw_complex runScheduledResponse The complex type name definition
 */
class runScheduledResponse {
}
/**
 * @pw_element int $id; // int
 * @pw_complex findServiceById The complex type name definition
 */
class findServiceById {
  public $id; // int
}
/**
 * @pw_element serviceEntity $return; // serviceEntity
 * @pw_complex findServiceByIdResponse The complex type name definition
 */
class findServiceByIdResponse {
  public $return; // serviceEntity
}
/**
 * @pw_element int $posInitial; // int
 * @pw_element int $posFinal; // int
 * @pw_element string $initialDate; // string
 * @pw_element string $finalDate; // string
 * @pw_element string $status; // string
 * @pw_complex findUploadedFiles The complex type name definition
 */
class findUploadedFiles {
  public $posInitial; // int
  public $posFinal; // int
  public $initialDate; // string
  public $finalDate; // string
  public $status; // string
}
/**
 * @pw_element filePojo $return; // filePojo
 * @pw_complex findUploadedFilesResponse The complex type name definition
 */
class findUploadedFilesResponse {
  public $return; // filePojo
}
/**
 * @pw_element string $comment; // string
 * @pw_element dateTime $createDate; // dateTime
 * @pw_element string $createUser; // string
 * @pw_element string $description; // string
 * @pw_element string $enconding; // string
 * @pw_element string $fileNameError; // string
 * @pw_element string $fileNameInput; // string
 * @pw_element string $fileNameOutput; // string
 * @pw_element string $fileType; // string
 * @pw_element int $id; // int
 * @pw_element dateTime $lastUpdateDate; // dateTime
 * @pw_element string $lastUpdateUser; // string
 * @pw_element int $numberOfAddresses; // int
 * @pw_element int $numberOfDefectiveRows; // int
 * @pw_element int $numberOfEmails; // int
 * @pw_element int $numberOfEmailsWithProblem; // int
 * @pw_element int $numberOfPhones; // int
 * @pw_element int $numberOfPhonesWithProblem; // int
 * @pw_element int $numberOfRows; // int
 * @pw_element int $numberOfSkippedRows; // int
 * @pw_element dateTime $postingDate; // dateTime
 * @pw_element long $processingTimeMsec; // long
 * @pw_element string $status; // string
 * @pw_element string $subject; // string
 * @pw_element boolean $success; // boolean
 * @pw_complex filePojo The complex type name definition
 */
class filePojo {
  public $comment; // string
  public $createDate; // dateTime
  public $createUser; // string
  public $description; // string
  public $enconding; // string
  public $fileNameError; // string
  public $fileNameInput; // string
  public $fileNameOutput; // string
  public $fileType; // string
  public $id; // int
  public $lastUpdateDate; // dateTime
  public $lastUpdateUser; // string
  public $numberOfAddresses; // int
  public $numberOfDefectiveRows; // int
  public $numberOfEmails; // int
  public $numberOfEmailsWithProblem; // int
  public $numberOfPhones; // int
  public $numberOfPhonesWithProblem; // int
  public $numberOfRows; // int
  public $numberOfSkippedRows; // int
  public $postingDate; // dateTime
  public $processingTimeMsec; // long
  public $status; // string
  public $subject; // string
  public $success; // boolean
}
/**
 * @pw_element string $messageCode; // string
 * @pw_complex messageWebReturn The complex type name definition
 */
class messageWebReturn {
  public $messageCode; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex messageWebReturnResponse The complex type name definition
 */
class messageWebReturnResponse {
  public $return; // string
}
/**
 * @pw_element string $alertInstanceCode; // string
 * @pw_complex cancelAlertInstance The complex type name definition
 */
class cancelAlertInstance {
  public $alertInstanceCode; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex cancelAlertInstanceResponse The complex type name definition
 */
class cancelAlertInstanceResponse {
  public $return; // string
}
/**
 * @pw_element string $schedulingDateTime; // string
 * @pw_element string $messageType; // string
 * @pw_element string $messageSubject; // string
 * @pw_element string $messageContent; // string
 * @pw_element int $idAlert; // int
 * @pw_element replacement $replacements; // replacement
 * @pw_element string $runCode; // string
 * @pw_complex newScheduling The complex type name definition
 */
class newScheduling {
  public $schedulingDateTime; // string
  public $messageType; // string
  public $messageSubject; // string
  public $messageContent; // string
  public $idAlert; // int
  public $replacements; // replacement
  public $runCode; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex newSchedulingResponse The complex type name definition
 */
class newSchedulingResponse {
  public $return; // string
}
/**
 * @pw_element int $idAlert; // int
 * @pw_element string $messageType; // string
 * @pw_element string $messageSubject; // string
 * @pw_element string $messageContent; // string
 * @pw_element replacement $replacements; // replacement
 * @pw_element string $runCode; // string
 * @pw_complex runFlow The complex type name definition
 */
class runFlow {
  public $idAlert; // int
  public $messageType; // string
  public $messageSubject; // string
  public $messageContent; // string
  public $replacements; // replacement
  public $runCode; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex runFlowResponse The complex type name definition
 */
class runFlowResponse {
  public $return; // string
}
/**
 * @pw_element int $id; // int
 * @pw_complex findUploadedFile The complex type name definition
 */
class findUploadedFile {
  public $id; // int
}
/**
 * @pw_element filePojo $return; // filePojo
 * @pw_complex findUploadedFileResponse The complex type name definition
 */
class findUploadedFileResponse {
  public $return; // filePojo
}
/**
 * @pw_element int $id; // int
 * @pw_element int $phase; // int
 * @pw_complex processBatch The complex type name definition
 */
class processBatch {
  public $id; // int
  public $phase; // int
}
/**
 * @pw_element boolean $return; // boolean
 * @pw_complex processBatchResponse The complex type name definition
 */
class processBatchResponse {
  public $return; // boolean
}
/**
 * @pw_element string $initialDate; // string
 * @pw_element string $finalDate; // string
 * @pw_element string $status; // string
 * @pw_complex countUploadedFiles The complex type name definition
 */
class countUploadedFiles {
  public $initialDate; // string
  public $finalDate; // string
  public $status; // string
}
/**
 * @pw_element long $return; // long
 * @pw_complex countUploadedFilesResponse The complex type name definition
 */
class countUploadedFilesResponse {
  public $return; // long
}
/**
 * @pw_element int $id; // int
 * @pw_complex discartUploadedFile The complex type name definition
 */
class discartUploadedFile {
  public $id; // int
}
/**
 * @pw_element boolean $return; // boolean
 * @pw_complex discartUploadedFileResponse The complex type name definition
 */
class discartUploadedFileResponse {
  public $return; // boolean
}
/**
 * @pw_element string $clientname; // string
 * @pw_element string $username; // string
 * @pw_element string $password; // string
 * @pw_element string $name; // string
 * @pw_element string $pinnumber; // string
 * @pw_element string $receptorgroupid; // string
 * @pw_complex registerReceptor The complex type name definition
 */
class registerReceptor {
  public $clientname; // string
  public $username; // string
  public $password; // string
  public $name; // string
  public $pinnumber; // string
  public $receptorgroupid; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex registerReceptorResponse The complex type name definition
 */
class registerReceptorResponse {
  public $return; // string
}
/**
 * @pw_element string $username; // string
 * @pw_element string $password; // string
 * @pw_element int $alertContent; // int
 * @pw_element int $maxRepetition; // int
 * @pw_element string $messageType; // string
 * @pw_element string $messageSubject; // string
 * @pw_element string $messageContent; // string
 * @pw_element personalAlertStep $personalAlertStep; // personalAlertStep
 * @pw_element int $idService; // int
 * @pw_element boolean $interactive; // boolean
 * @pw_complex createPersonalAlertPlus The complex type name definition
 */
class createPersonalAlertPlus {
  public $username; // string
  public $password; // string
  public $alertContent; // int
  public $maxRepetition; // int
  public $messageType; // string
  public $messageSubject; // string
  public $messageContent; // string
  public $personalAlertStep; // personalAlertStep
  public $idService; // int
  public $interactive; // boolean
}
/**
 * @pw_element string $address; // string
 * @pw_element string $channel; // string
 * @pw_element int $id; // int
 * @pw_element personalAlertConditional $personalAlertConditional; // personalAlertConditional
 * @pw_element string $stepContent; // string
 * @pw_element string $stepSubject; // string
 * @pw_element string $stepType; // string
 * @pw_element string $timeToWait; // string
 * @pw_complex personalAlertStep The complex type name definition
 */
class personalAlertStep {
  public $address; // string
  public $channel; // string
  public $id; // int
  public $personalAlertConditional; // personalAlertConditional
  public $stepContent; // string
  public $stepSubject; // string
  public $stepType; // string
  public $timeToWait; // string
}
/**
 * @pw_element int $nextPersonalAlertStepId; // int
 * @pw_element string $responseRegex; // string
 * @pw_element boolean $stopIfSuccess; // boolean
 * @pw_element boolean $timeoutOccured; // boolean
 * @pw_complex personalAlertConditional The complex type name definition
 */
class personalAlertConditional {
  public $nextPersonalAlertStepId; // int
  public $responseRegex; // string
  public $stopIfSuccess; // boolean
  public $timeoutOccured; // boolean
}
/**
 * @pw_element string $return; // string
 * @pw_complex createPersonalAlertPlusResponse The complex type name definition
 */
class createPersonalAlertPlusResponse {
  public $return; // string
}
/**
 * @pw_element string $hour; // string
 * @pw_element string $minute; // string
 * @pw_element string $second; // string
 * @pw_complex setTimerFileUpload The complex type name definition
 */
class setTimerFileUpload {
  public $hour; // string
  public $minute; // string
  public $second; // string
}
/**
 * @pw_complex setTimerFileUploadResponse The complex type name definition
 */
class setTimerFileUploadResponse {
}
/**
 * @pw_element string $schedulingDateTime; // string
 * @pw_element string $messageType; // string
 * @pw_element string $messageSubject; // string
 * @pw_element string $messageContent; // string
 * @pw_element int $receptorgroupid; // int
 * @pw_element replacement $replacements; // replacement
 * @pw_element string $runCode; // string
 * @pw_complex schedulePersonalAlertByReceptorGroup The complex type name definition
 */
class schedulePersonalAlertByReceptorGroup {
  public $schedulingDateTime; // string
  public $messageType; // string
  public $messageSubject; // string
  public $messageContent; // string
  public $receptorgroupid; // int
  public $replacements; // replacement
  public $runCode; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex schedulePersonalAlertByReceptorGroupResponse The complex type name definition
 */
class schedulePersonalAlertByReceptorGroupResponse {
  public $return; // string
}
/**
 * @pw_element int $idReceptorGroup; // int
 * @pw_element string $receptorGroupName; // string
 * @pw_complex editReceptorGroup The complex type name definition
 */
class editReceptorGroup {
  public $idReceptorGroup; // int
  public $receptorGroupName; // string
}
/**
 * @pw_element receptorGroupEntity $return; // receptorGroupEntity
 * @pw_complex editReceptorGroupResponse The complex type name definition
 */
class editReceptorGroupResponse {
  public $return; // receptorGroupEntity
}
/**
 * @pw_element string $runCode; // string
 * @pw_complex cancelAlertInstanceByRunCode The complex type name definition
 */
class cancelAlertInstanceByRunCode {
  public $runCode; // string
}
/**
 * @pw_element int $return; // int
 * @pw_complex cancelAlertInstanceByRunCodeResponse The complex type name definition
 */
class cancelAlertInstanceByRunCodeResponse {
  public $return; // int
}
/**
 * @pw_element int $idReceptorGroup; // int
 * @pw_complex getReceptorGroupById The complex type name definition
 */
class getReceptorGroupById {
  public $idReceptorGroup; // int
}
/**
 * @pw_element receptorGroupEntity $return; // receptorGroupEntity
 * @pw_complex getReceptorGroupByIdResponse The complex type name definition
 */
class getReceptorGroupByIdResponse {
  public $return; // receptorGroupEntity
}
/**
 * @pw_element string $alertInstanceCode; // string
 * @pw_complex getRSVPByAlertInstanceCode The complex type name definition
 */
class getRSVPByAlertInstanceCode {
  public $alertInstanceCode; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex getRSVPByAlertInstanceCodeResponse The complex type name definition
 */
class getRSVPByAlertInstanceCodeResponse {
  public $return; // string
}
/**
 * @pw_element string $username; // string
 * @pw_element string $password; // string
 * @pw_complex getPersonalAlert The complex type name definition
 */
class getPersonalAlert {
  public $username; // string
  public $password; // string
}
/**
 * @pw_element personalAlertReturn $return; // personalAlertReturn
 * @pw_complex getPersonalAlertResponse The complex type name definition
 */
class getPersonalAlertResponse {
  public $return; // personalAlertReturn
}
/**
 * @pw_element alertEntity $alert; // alertEntity
 * @pw_element alertStepEntity $alertStepsCollection; // alertStepEntity
 * @pw_element receptorEntity $receptor; // receptorEntity
 * @pw_complex personalAlertReturn The complex type name definition
 */
class personalAlertReturn {
  public $alert; // alertEntity
  public $alertStepsCollection; // alertStepEntity
  public $receptor; // receptorEntity
}
/**
 * @pw_element alertContentEntity $alertContent; // alertContentEntity
 * @pw_element string $alertDescription; // string
 * @pw_element string $alertName; // string
 * @pw_element formatGroupEntity $formatGroup; // formatGroupEntity
 * @pw_element int $id; // int
 * @pw_element boolean $interactive; // boolean
 * @pw_element int $maxRepetition; // int
 * @pw_element receptorEntity $receptor; // receptorEntity
 * @pw_element serviceEntity $service; // serviceEntity
 * @pw_element long $version; // long
 * @pw_complex alertEntity The complex type name definition
 */
class alertEntity {
  public $alertContent; // alertContentEntity
  public $alertDescription; // string
  public $alertName; // string
  public $formatGroup; // formatGroupEntity
  public $id; // int
  public $interactive; // boolean
  public $maxRepetition; // int
  public $receptor; // receptorEntity
  public $service; // serviceEntity
  public $version; // long
}
/**
 * @pw_element string $alertContent; // string
 * @pw_element string $alertSubject; // string
 * @pw_element string $alertType; // string
 * @pw_element int $id; // int
 * @pw_element long $version; // long
 * @pw_complex alertContentEntity The complex type name definition
 */
class alertContentEntity {
  public $alertContent; // string
  public $alertSubject; // string
  public $alertType; // string
  public $id; // int
  public $version; // long
}
/**
 * @pw_element dateTime $createDate; // dateTime
 * @pw_element scUserEntity $createUser; // scUserEntity
 * @pw_element string $formatGroupDescription; // string
 * @pw_element string $formatGroupName; // string
 * @pw_element long $id; // long
 * @pw_element dateTime $lastUpdateDate; // dateTime
 * @pw_element scUserEntity $lastUpdateUser; // scUserEntity
 * @pw_element long $version; // long
 * @pw_complex formatGroupEntity The complex type name definition
 */
class formatGroupEntity {
  public $createDate; // dateTime
  public $createUser; // scUserEntity
  public $formatGroupDescription; // string
  public $formatGroupName; // string
  public $id; // long
  public $lastUpdateDate; // dateTime
  public $lastUpdateUser; // scUserEntity
  public $version; // long
}
/**
 * @pw_element alertEntity $alert; // alertEntity
 * @pw_element int $id; // int
 * @pw_element string $login; // string
 * @pw_element string $name; // string
 * @pw_element string $password; // string
 * @pw_element long $version; // long
 * @pw_complex scUserEntity The complex type name definition
 */
class scUserEntity {
  public $alert; // alertEntity
  public $id; // int
  public $login; // string
  public $name; // string
  public $password; // string
  public $version; // long
}
/**
 * @pw_element alertEntity $alertDefault; // alertEntity
 * @pw_element int $id; // int
 * @pw_element string $pinnumber; // string
 * @pw_element string $receptorDescription; // string
 * @pw_element receptorGroupEntity $receptorGroup; // receptorGroupEntity
 * @pw_element string $receptorName; // string
 * @pw_element string $receptorPassword; // string
 * @pw_element string $receptorType; // string
 * @pw_element string $receptorUserName; // string
 * @pw_element long $version; // long
 * @pw_complex receptorEntity The complex type name definition
 */
class receptorEntity {
  public $alertDefault; // alertEntity
  public $id; // int
  public $pinnumber; // string
  public $receptorDescription; // string
  public $receptorGroup; // receptorGroupEntity
  public $receptorName; // string
  public $receptorPassword; // string
  public $receptorType; // string
  public $receptorUserName; // string
  public $version; // long
}
/**
 * @pw_element string $alertStepName; // string
 * @pw_element long $alertStepWaitTime; // long
 * @pw_element channelReceptorEntity $channelReceptor; // channelReceptorEntity
 * @pw_element int $id; // int
 * @pw_element string $messageContent; // string
 * @pw_element string $messageSubject; // string
 * @pw_element string $messageType; // string
 * @pw_element long $version; // long
 * @pw_complex alertStepEntity The complex type name definition
 */
class alertStepEntity {
  public $alertStepName; // string
  public $alertStepWaitTime; // long
  public $channelReceptor; // channelReceptorEntity
  public $id; // int
  public $messageContent; // string
  public $messageSubject; // string
  public $messageType; // string
  public $version; // long
}
/**
 * @pw_element string $addresses; // string
 * @pw_element channelEntity $channel; // channelEntity
 * @pw_element int $id; // int
 * @pw_element receptorEntity $receptor; // receptorEntity
 * @pw_element long $version; // long
 * @pw_complex channelReceptorEntity The complex type name definition
 */
class channelReceptorEntity {
  public $addresses; // string
  public $channel; // channelEntity
  public $id; // int
  public $receptor; // receptorEntity
  public $version; // long
}
/**
 * @pw_element billingCategoryEntity $billingCategory; // billingCategoryEntity
 * @pw_element string $channelDescription; // string
 * @pw_element string $channelName; // string
 * @pw_element int $idchannel; // int
 * @pw_element long $version; // long
 * @pw_complex channelEntity The complex type name definition
 */
class channelEntity {
  public $billingCategory; // billingCategoryEntity
  public $channelDescription; // string
  public $channelName; // string
  public $idchannel; // int
  public $version; // long
}
/**
 * @pw_element string $billingCategoryDescription; // string
 * @pw_element string $billingCategoryName; // string
 * @pw_element string $billingCategoryRule; // string
 * @pw_element billingTransactionTypeEntity $billingCategoryTransactionType; // billingTransactionTypeEntity
 * @pw_element billingTransactionTypeEntity $billingCategoryTransactionTypeConfirm; // billingTransactionTypeEntity
 * @pw_element billingTransactionTypeEntity $billingCategoryTransactionTypeReturn; // billingTransactionTypeEntity
 * @pw_element dateTime $createDate; // dateTime
 * @pw_element scUserEntity $createUser; // scUserEntity
 * @pw_element long $id; // long
 * @pw_element dateTime $lastUpdateDate; // dateTime
 * @pw_element scUserEntity $lastUpdateUser; // scUserEntity
 * @pw_element long $version; // long
 * @pw_complex billingCategoryEntity The complex type name definition
 */
class billingCategoryEntity {
  public $billingCategoryDescription; // string
  public $billingCategoryName; // string
  public $billingCategoryRule; // string
  public $billingCategoryTransactionType; // billingTransactionTypeEntity
  public $billingCategoryTransactionTypeConfirm; // billingTransactionTypeEntity
  public $billingCategoryTransactionTypeReturn; // billingTransactionTypeEntity
  public $createDate; // dateTime
  public $createUser; // scUserEntity
  public $id; // long
  public $lastUpdateDate; // dateTime
  public $lastUpdateUser; // scUserEntity
  public $version; // long
}
/**
 * @pw_element string $billingTransactionCommittedInventory; // string
 * @pw_element string $billingTransactionContractedInventory; // string
 * @pw_element string $billingTransactionRegister; // string
 * @pw_element string $billingTransactionTypeDescription; // string
 * @pw_element string $billingTransactionTypeName; // string
 * @pw_element string $billingTransactionUsedInventory; // string
 * @pw_element dateTime $createDate; // dateTime
 * @pw_element scUserEntity $createUser; // scUserEntity
 * @pw_element long $id; // long
 * @pw_element dateTime $lastUpdateDate; // dateTime
 * @pw_element scUserEntity $lastUpdateUser; // scUserEntity
 * @pw_element long $version; // long
 * @pw_complex billingTransactionTypeEntity The complex type name definition
 */
class billingTransactionTypeEntity {
  public $billingTransactionCommittedInventory; // string
  public $billingTransactionContractedInventory; // string
  public $billingTransactionRegister; // string
  public $billingTransactionTypeDescription; // string
  public $billingTransactionTypeName; // string
  public $billingTransactionUsedInventory; // string
  public $createDate; // dateTime
  public $createUser; // scUserEntity
  public $id; // long
  public $lastUpdateDate; // dateTime
  public $lastUpdateUser; // scUserEntity
  public $version; // long
}
/**
 * @pw_element string $name; // string
 * @pw_element string $description; // string
 * @pw_element string $origin; // string
 * @pw_element string $workdays; // string
 * @pw_element string $workingHours; // string
 * @pw_element boolean $delayOfOneDayInTheFirstStep; // boolean
 * @pw_element string $callBackUrl; // string
 * @pw_element string $callBackUser; // string
 * @pw_element string $callBackPassword; // string
 * @pw_complex createService The complex type name definition
 */
class createService {
  public $name; // string
  public $description; // string
  public $origin; // string
  public $workdays; // string
  public $workingHours; // string
  public $delayOfOneDayInTheFirstStep; // boolean
  public $callBackUrl; // string
  public $callBackUser; // string
  public $callBackPassword; // string
}
/**
 * @pw_element int $return; // int
 * @pw_complex createServiceResponse // Service id froM created Service
 */
class createServiceResponse {
  public $return; // int
}
/**
 * @pw_element string $messageCode; // string
 * @pw_element string $messageResponse; // string
 * @pw_complex messageWebResponse The complex type name definition
 */
class messageWebResponse {
  public $messageCode; // string
  public $messageResponse; // string
}
/**
 * @pw_element string $return; // string
 * @pw_complex messageWebResponseResponse The complex type name definition
 */
class messageWebResponseResponse {
  public $return; // string
}
/**
 * @pw_element int $id; // int
 * @pw_element int $idEmpresa; // int
 * @pw_complex importUploadedFile The complex type name definition
 */
class importUploadedFile {
  public $id; // int
  public $idEmpresa; // int
}
/**
 * @pw_element boolean $return; // boolean
 * @pw_complex importUploadedFileResponse The complex type name definition
 */
class importUploadedFileResponse {
  public $return; // boolean
}
/**
 * @pw_element string $runCode; // string
 * @pw_complex runExecuted The complex type name definition
 */
class runExecuted {
  public $runCode; // string
}
/**
 * @pw_complex runExecuted The complex type name definition
 */
class runExecutedResponse {
}
/**
 * @pw_element string $subject; // string
 * @pw_element string $description; // string
 * @pw_element string $schedulingDateTime; // string
 * @pw_element int $filterId; // int
 * @pw_element int $serviceId; // int
 * @pw_element string $sqlQuery; // string
 * @pw_element boolean $interactive; // boolean
 * @pw_complex createBatch The complex type name definition
 */
class createBatch {
  public $subject; // string
  public $description; // string
  public $schedulingDateTime; // string
  public $filterId; // int
  public $serviceId; // int
  public $sqlQuery; // string
  public $interactive; // boolean
}
/**
 * @pw_element batchPojo $return; // batchPojo
 * @pw_complex createBatchResponse The complex type name definition
 */
class createBatchResponse {
  public $return; // batchPojo
}
/**
 * @pw_element string $initialDate; // string
 * @pw_element string $finalDate; // string
 * @pw_complex countBatches The complex type name definition
 */
class countBatches {
  public $initialDate; // string
  public $finalDate; // string
}
/**
 * @pw_element long $return; // long
 * @pw_complex countBatchesResponse The complex type name definition
 */
class countBatchesResponse {
  public $return; // long
}
/**
 * @pw_element int $id; // int
 * @pw_element string $subject; // string
 * @pw_element string $description; // string
 * @pw_complex updateUploadedFile The complex type name definition
 */
class updateUploadedFile {
  public $id; // int
  public $subject; // string
  public $description; // string
}
/**
 * @pw_element filePojo $return; // filePojo
 * @pw_complex updateUploadedFileResponse The complex type name definition
 */
class updateUploadedFileResponse {
  public $return; // filePojo
}



/**
* @pw_complex Array The complex type name definition
*/


