<?php
/**
 * @service PersonalAlertServiceSoapClient
 */
class PersonalAlertServiceSoapClient{
	/**
	 * The WSDL URI
	 *
	 * @var string
	 */
	public static $_WsdlUri='http://curupira.local:9999/PersonalAlertService.php?WSDL';
	/**
	 * The PHP SoapClient object
	 *
	 * @var object
	 */
	public static $_Server=null;

	/**
	 * Send a SOAP request to the server
	 *
	 * @param string $method The method name
	 * @param array $param The parameters
	 * @return mixed The server response
	 */
	public static function _Call($method,$param){
		if(is_null(self::$_Server))
			self::$_Server=new SoapClient(self::$_WsdlUri);
		return self::$_Server->__soapCall($method,$param);
	}

	/**
	 * @param createService $parameters
	 * @return createServiceResponse
	 */
	public function createService($parameters){
		return self::_Call('createService',Array(
			$parameters
		));
	}

	/**
	 * @param messageWebReturn $parameters
	 * @return messageWebReturnResponse
	 */
	public function messageWebReturn($parameters){
		return self::_Call('messageWebReturn',Array(
			$parameters
		));
	}

	/**
	 * @param messageWebResponse $parameters
	 * @return messageWebResponseResponse
	 */
	public function messageWebResponse($parameters){
		return self::_Call('messageWebResponse',Array(
			$parameters
		));
	}

	/**
	 * @param runExecuted $parameters
	 * @return runExecutedResponse
	 */
	public function runExecuted($parameters){
		return self::_Call('runExecuted',Array(
			$parameters
		));
	}

	/**
	 * @param runScheduled $parameters
	 * @return runScheduledResponse
	 */
	public function runScheduled($parameters){
		return self::_Call('runScheduled',Array(
			$parameters
		));
	}

	/**
	 * @param getAllStepIntanceByAlertInstanceCode $parameters
	 * @return getAllStepIntanceByAlertInstanceCodeResponse
	 */
	public function getAllStepIntanceByAlertInstanceCode($parameters){
		return self::_Call('getAllStepIntanceByAlertInstanceCode',Array(
			$parameters
		));
	}

	/**
	 * @param getPersonalAlert $parameters
	 * @return getPersonalAlertResponse
	 */
	public function getPersonalAlert($parameters){
		return self::_Call('getPersonalAlert',Array(
			$parameters
		));
	}

	/**
	 * @param createPersonalAlert $parameters
	 * @return createPersonalAlertResponse
	 */
	public function createPersonalAlert($parameters){
		return self::_Call('createPersonalAlert',Array(
			$parameters
		));
	}

	/**
	 * @param cancelAlertInstance $parameters
	 * @return cancelAlertInstanceResponse
	 */
	public function cancelAlertInstance($parameters){
		return self::_Call('cancelAlertInstance',Array(
			$parameters
		));
	}

	/**
	 * @param runFlow $parameters
	 * @return runFlowResponse
	 */
	public function runFlow($parameters){
		return self::_Call('runFlow',Array(
			$parameters
		));
	}

	/**
	 * @param getRSVPByAlertInstanceCode $parameters
	 * @return getRSVPByAlertInstanceCodeResponse
	 */
	public function getRSVPByAlertInstanceCode($parameters){
		return self::_Call('getRSVPByAlertInstanceCode',Array(
			$parameters
		));
	}

	/**
	 * @param newScheduling $parameters
	 * @return newSchedulingResponse
	 */
	public function newScheduling($parameters){
		return self::_Call('newScheduling',Array(
			$parameters
		));
	}

	/**
	 * @param schedulePersonalAlertByReceptorGroup $parameters
	 * @return schedulePersonalAlertByReceptorGroupResponse
	 */
	public function schedulePersonalAlertByReceptorGroup($parameters){
		return self::_Call('schedulePersonalAlertByReceptorGroup',Array(
			$parameters
		));
	}

	/**
	 * @param editScheduling $parameters
	 * @return editSchedulingResponse
	 */
	public function editScheduling($parameters){
		return self::_Call('editScheduling',Array(
			$parameters
		));
	}

	/**
	 * @param removeScheduling $parameters
	 * @return removeSchedulingResponse
	 */
	public function removeScheduling($parameters){
		return self::_Call('removeScheduling',Array(
			$parameters
		));
	}

	/**
	 * @param generateRunCode $parameters
	 * @return generateRunCodeResponse
	 */
	public function generateRunCode($parameters){
		return self::_Call('generateRunCode',Array(
			$parameters
		));
	}

	/**
	 * @param cancelAlertInstanceByRunCode $parameters
	 * @return cancelAlertInstanceByRunCodeResponse
	 */
	public function cancelAlertInstanceByRunCode($parameters){
		return self::_Call('cancelAlertInstanceByRunCode',Array(
			$parameters
		));
	}

	/**
	 * @param cancelSchedullingByRunCode $parameters
	 * @return cancelSchedullingByRunCodeResponse
	 */
	public function cancelSchedullingByRunCode($parameters){
		return self::_Call('cancelSchedullingByRunCode',Array(
			$parameters
		));
	}

	/**
	 * @param setTimerFileUpload $parameters
	 * @return setTimerFileUploadResponse
	 */
	public function setTimerFileUpload($parameters){
		return self::_Call('setTimerFileUpload',Array(
			$parameters
		));
	}

	/**
	 * @param countUploadedFiles $parameters
	 * @return countUploadedFilesResponse
	 */
	public function countUploadedFiles($parameters){
		return self::_Call('countUploadedFiles',Array(
			$parameters
		));
	}

	/**
	 * @param findUploadedFiles $parameters
	 * @return findUploadedFilesResponse
	 */
	public function findUploadedFiles($parameters){
		return self::_Call('findUploadedFiles',Array(
			$parameters
		));
	}

	/**
	 * @param findUploadedFile $parameters
	 * @return findUploadedFileResponse
	 */
	public function findUploadedFile($parameters){
		return self::_Call('findUploadedFile',Array(
			$parameters
		));
	}

	/**
	 * @param updateUploadedFile $parameters
	 * @return updateUploadedFileResponse
	 */
	public function updateUploadedFile($parameters){
		return self::_Call('updateUploadedFile',Array(
			$parameters
		));
	}

	/**
	 * @param discartUploadedFile $parameters
	 * @return discartUploadedFileResponse
	 */
	public function discartUploadedFile($parameters){
		return self::_Call('discartUploadedFile',Array(
			$parameters
		));
	}

	/**
	 * @param importUploadedFile $parameters
	 * @return importUploadedFileResponse
	 */
	public function importUploadedFile($parameters){
		return self::_Call('importUploadedFile',Array(
			$parameters
		));
	}

	/**
	 * @param countBatches $parameters
	 * @return countBatchesResponse
	 */
	public function countBatches($parameters){
		return self::_Call('countBatches',Array(
			$parameters
		));
	}

	/**
	 * @param getReceptorGroupByName $parameters
	 * @return getReceptorGroupByNameResponse
	 */
	public function getReceptorGroupByName($parameters){
		return self::_Call('getReceptorGroupByName',Array(
			$parameters
		));
	}

	/**
	 * @param registerReceptor $parameters
	 * @return registerReceptorResponse
	 */
	public function registerReceptor($parameters){
		return self::_Call('registerReceptor',Array(
			$parameters
		));
	}

	/**
	 * @param createPersonalAlertPlus $parameters
	 * @return createPersonalAlertPlusResponse
	 */
	public function createPersonalAlertPlus($parameters){
		return self::_Call('createPersonalAlertPlus',Array(
			$parameters
		));
	}

	/**
	 * @param removePersonalAlert $parameters
	 * @return removePersonalAlertResponse
	 */
	public function removePersonalAlert($parameters){
		return self::_Call('removePersonalAlert',Array(
			$parameters
		));
	}

	/**
	 * @param findBatches $parameters
	 * @return findBatchesResponse
	 */
	public function findBatches($parameters){
		return self::_Call('findBatches',Array(
			$parameters
		));
	}

	/**
	 * @param findBatch $parameters
	 * @return findBatchResponse
	 */
	public function findBatch($parameters){
		return self::_Call('findBatch',Array(
			$parameters
		));
	}

	/**
	 * @param updateBatch $parameters
	 * @return updateBatchResponse
	 */
	public function updateBatch($parameters){
		return self::_Call('updateBatch',Array(
			$parameters
		));
	}

	/**
	 * @param createBatch $parameters
	 * @return createBatchResponse
	 */
	public function createBatch($parameters){
		return self::_Call('createBatch',Array(
			$parameters
		));
	}

	/**
	 * @param processBatch $parameters
	 * @return processBatchResponse
	 */
	public function processBatch($parameters){
		return self::_Call('processBatch',Array(
			$parameters
		));
	}

	/**
	 * @param getReceptorGroupById $parameters
	 * @return getReceptorGroupByIdResponse
	 */
	public function getReceptorGroupById($parameters){
		return self::_Call('getReceptorGroupById',Array(
			$parameters
		));
	}

	/**
	 * @param findServiceById $parameters
	 * @return findServiceByIdResponse
	 */
	public function findServiceById($parameters){
		return self::_Call('findServiceById',Array(
			$parameters
		));
	}

	/**
	 * @param discartBatch $parameters
	 * @return discartBatchResponse
	 */
	public function discartBatch($parameters){
		return self::_Call('discartBatch',Array(
			$parameters
		));
	}

	/**
	 * @param findAllServices $parameters
	 * @return findAllServicesResponse
	 */
	public function findAllServices($parameters){
		return self::_Call('findAllServices',Array(
			$parameters
		));
	}

	/**
	 * @param findRangeOfServices $parameters
	 * @return findRangeOfServicesResponse
	 */
	public function findRangeOfServices($parameters){
		return self::_Call('findRangeOfServices',Array(
			$parameters
		));
	}

	/**
	 * @param editService $parameters
	 * @return editServiceResponse
	 */
	public function editService($parameters){
		return self::_Call('editService',Array(
			$parameters
		));
	}

	/**
	 * @param editReceptorGroup $parameters
	 * @return editReceptorGroupResponse
	 */
	public function editReceptorGroup($parameters){
		return self::_Call('editReceptorGroup',Array(
			$parameters
		));
	}

	/**
	 * @param findServiceByName $parameters
	 * @return findServiceByNameResponse
	 */
	public function findServiceByName($parameters){
		return self::_Call('findServiceByName',Array(
			$parameters
		));
	}

	/**
	 * @param newReceptorGroup $parameters
	 * @return newReceptorGroupResponse
	 */
	public function newReceptorGroup($parameters){
		return self::_Call('newReceptorGroup',Array(
			$parameters
		));
	}
}

/**
 * The complex type name definition
 *
 * @pw_element string $name A string with a value
 * @pw_complex findServiceByName
 */
class findServiceByName{
	/**
	 * A string with a value
	 *
	 * @var string
	 */
	public $name;
}

/**
 * The complex type name definition
 *
 * @pw_element serviceEntity $return A serviceEntity with a value
 * @pw_complex findServiceByNameResponse
 */
class findServiceByNameResponse{
	/**
	 * A serviceEntity with a value
	 *
	 * @var serviceEntity
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $callBackPassword // string
 * @pw_element string $callBackUrl // string
 * @pw_element string $callBackUser // string
 * @pw_element boolean $delayOfOneDayInTheFirstStep // boolean
 * @pw_element string $description // string
 * @pw_element int $id // int
 * @pw_element string $name // string
 * @pw_element string $origin // string
 * @pw_element long $version // long
 * @pw_element string $workdays // string
 * @pw_element string $workingHours // string
 * @pw_complex serviceEntity
 */
class serviceEntity{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $callBackPassword;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $callBackUrl;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $callBackUser;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $delayOfOneDayInTheFirstStep;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $description;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $name;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $origin;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $workdays;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $workingHours;
}

/**
 * The complex type name definition
 *
 * @pw_element string $detail // string
 * @pw_element string $faultcode // string
 * @pw_element string $faultstring // string
 * @pw_complex paprExceptionDetail
 */
class paprExceptionDetail{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $detail;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $faultcode;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $faultstring;
}

/**
 * The complex type name definition
 *
 * @pw_element string $name // string
 * @pw_complex getReceptorGroupByName
 */
class getReceptorGroupByName{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $name;
}

/**
 * The complex type name definition
 *
 * @pw_element receptorGroupEntity $return // receptorGroupEntity
 * @pw_complex getReceptorGroupByNameResponse
 */
class getReceptorGroupByNameResponse{
	/**
	 * // receptorGroupEntity
	 *
	 * @var receptorGroupEntity
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $id // int
 * @pw_element string $receptorGroupName // string
 * @pw_element long $version // long
 * @pw_complex receptorGroupEntity
 */
class receptorGroupEntity{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $receptorGroupName;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element string $alertInstanceCode // string
 * @pw_complex getAllStepIntanceByAlertInstanceCode
 */
class getAllStepIntanceByAlertInstanceCode{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertInstanceCode;
}

/**
 * The complex type name definition
 *
 * @pw_element stepInstanceEntity $return // stepInstanceEntity
 * @pw_complex getAllStepIntanceByAlertInstanceCodeResponse
 */
class getAllStepIntanceByAlertInstanceCodeResponse{
	/**
	 * // stepInstanceEntity
	 *
	 * @var stepInstanceEntity
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element alertInstanceEntity $alertInstance // alertInstanceEntity
 * @pw_element boolean $calledBack // boolean
 * @pw_element dateTime $createDate // dateTime
 * @pw_element int $createUser // int
 * @pw_element int $id // int
 * @pw_element int $idAlertStep // int
 * @pw_element string $messageContent // string
 * @pw_element string $messageSubject // string
 * @pw_element string $messageType // string
 * @pw_element int $numberOfRepetition // int
 * @pw_element string $stepContent // string
 * @pw_element string $stepMessageCode // string
 * @pw_element string $stepMessageStatus // string
 * @pw_element string $stepReceptor // string
 * @pw_element string $stepReceptorAddress // string
 * @pw_element dateTime $stepReplyDatetime // dateTime
 * @pw_element unsignedShort $stepReplyStatus // unsignedShort
 * @pw_element dateTime $stepSendDatetime // dateTime
 * @pw_element long $version // long
 * @pw_complex stepInstanceEntity
 */
class stepInstanceEntity{
	/**
	 * // alertInstanceEntity
	 *
	 * @var alertInstanceEntity
	 */
	public $alertInstance;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $calledBack;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $createDate;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $createUser;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $idAlertStep;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageContent;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageSubject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageType;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $numberOfRepetition;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $stepContent;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $stepMessageCode;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $stepMessageStatus;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $stepReceptor;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $stepReceptorAddress;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $stepReplyDatetime;
	/**
	 * // unsignedShort
	 *
	 * @var unsignedShort
	 */
	public $stepReplyStatus;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $stepSendDatetime;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element int $alertIdalert // int
 * @pw_element string $alertInstanceCode // string
 * @pw_element string $comment // string
 * @pw_element dateTime $createDate // dateTime
 * @pw_element long $createUser // long
 * @pw_element dateTime $emissionDateTime // dateTime
 * @pw_element int $id // int
 * @pw_element string $messageContent // string
 * @pw_element string $messageSubject // string
 * @pw_element string $messageType // string
 * @pw_element string $rsvp // string
 * @pw_element string $runCode // string
 * @pw_element string $serviceDescription // string
 * @pw_element string $serviceName // string
 * @pw_element string $serviceOrigin // string
 * @pw_element unsignedShort $status // unsignedShort
 * @pw_element long $version // long
 * @pw_complex alertInstanceEntity
 */
class alertInstanceEntity{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $alertIdalert;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertInstanceCode;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $comment;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $createDate;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $createUser;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $emissionDateTime;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageContent;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageSubject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageType;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $rsvp;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $runCode;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $serviceDescription;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $serviceName;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $serviceOrigin;
	/**
	 * // unsignedShort
	 *
	 * @var unsignedShort
	 */
	public $status;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element string $runCode // string
 * @pw_complex cancelSchedullingByRunCode
 */
class cancelSchedullingByRunCode{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $runCode;
}

/**
 * The complex type name definition
 *
 * @pw_element int $return // int
 * @pw_complex cancelSchedullingByRunCodeResponse
 */
class cancelSchedullingByRunCodeResponse{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $receptorGroupName // string
 * @pw_complex newReceptorGroup
 */
class newReceptorGroup{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $receptorGroupName;
}

/**
 * The complex type name definition
 *
 * @pw_element int $return // int
 * @pw_complex newReceptorGroupResponse
 */
class newReceptorGroupResponse{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $id // int
 * @pw_element string $nameNew // string
 * @pw_element string $descriptionNew // string
 * @pw_element string $originNew // string
 * @pw_element string $workdaysNew // string
 * @pw_element string $workingHoursNew // string
 * @pw_element boolean $delayOfOneDayInTheFirstStepNew // boolean
 * @pw_element string $callBackUrlNew // string
 * @pw_element string $callBackUserNew // string
 * @pw_element string $callBackPasswordNew // string
 * @pw_complex editService
 */
class editService{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $nameNew;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $descriptionNew;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $originNew;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $workdaysNew;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $workingHoursNew;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $delayOfOneDayInTheFirstStepNew;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $callBackUrlNew;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $callBackUserNew;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $callBackPasswordNew;
}

/**
 * The complex type name definition
 *
 * @pw_element serviceEntity $return // serviceEntity
 * @pw_complex editServiceResponse
 */
class editServiceResponse{
	/**
	 * // serviceEntity
	 *
	 * @var serviceEntity
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $id // int
 * @pw_element string $subject // string
 * @pw_element string $description // string
 * @pw_element string $schedulingDateTime // string
 * @pw_element int $filterId // int
 * @pw_element int $serviceId // int
 * @pw_element string $sqlQuery // string
 * @pw_complex updateBatch
 */
class updateBatch{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $subject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $description;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $schedulingDateTime;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $filterId;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $serviceId;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $sqlQuery;
}

/**
 * The complex type name definition
 *
 * @pw_element batchPojo $return // batchPojo
 * @pw_complex updateBatchResponse
 */
class updateBatchResponse{
	/**
	 * // batchPojo
	 *
	 * @var batchPojo
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element dateTime $createDate // dateTime
 * @pw_element boolean $createPersonalAlert // boolean
 * @pw_element dateTime $createPersonalAlertDateEnded // dateTime
 * @pw_element dateTime $createPersonalAlertDateStarted // dateTime
 * @pw_element long $createPersonalAlertElapsedTime // long
 * @pw_element int $createPersonalAlertRecordsProcessed // int
 * @pw_element string $createUser // string
 * @pw_element string $description // string
 * @pw_element int $filterId // int
 * @pw_element int $id // int
 * @pw_element dateTime $lastUpdateDate // dateTime
 * @pw_element string $lastUpdateUser // string
 * @pw_element boolean $linkService // boolean
 * @pw_element dateTime $linkServiceDateEnded // dateTime
 * @pw_element dateTime $linkServiceDateStarted // dateTime
 * @pw_element long $linkServiceElapsedTime // long
 * @pw_element int $linkServiceRecordsProcessed // int
 * @pw_element boolean $registerReceptor // boolean
 * @pw_element dateTime $registerReceptorDateEnded // dateTime
 * @pw_element dateTime $registerReceptorDateStarted // dateTime
 * @pw_element long $registerReceptorElapsedTime // long
 * @pw_element int $registerReceptorRecordsProcessed // int
 * @pw_element boolean $run // boolean
 * @pw_element dateTime $runDateEnded // dateTime
 * @pw_element dateTime $runDateStarted // dateTime
 * @pw_element long $runElapsedTime // long
 * @pw_element int $runRecordsProcessed // int
 * @pw_element string $runType // string
 * @pw_element string $runcode // string
 * @pw_element dateTime $schedulingDateTime // dateTime
 * @pw_element int $serviceId // int
 * @pw_element string $sqlQuery // string
 * @pw_element string $status // string
 * @pw_element string $subject // string
 * @pw_element int $totalRecords // int
 * @pw_complex batchPojo
 */
class batchPojo{
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $createDate;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $createPersonalAlert;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $createPersonalAlertDateEnded;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $createPersonalAlertDateStarted;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $createPersonalAlertElapsedTime;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $createPersonalAlertRecordsProcessed;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $createUser;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $description;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $filterId;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $lastUpdateDate;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $lastUpdateUser;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $linkService;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $linkServiceDateEnded;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $linkServiceDateStarted;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $linkServiceElapsedTime;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $linkServiceRecordsProcessed;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $registerReceptor;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $registerReceptorDateEnded;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $registerReceptorDateStarted;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $registerReceptorElapsedTime;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $registerReceptorRecordsProcessed;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $run;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $runDateEnded;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $runDateStarted;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $runElapsedTime;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $runRecordsProcessed;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $runType;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $runcode;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $schedulingDateTime;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $serviceId;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $sqlQuery;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $status;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $subject;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $totalRecords;
}

/**
 * The complex type name definition
 *
 * @pw_element int $posInitial // int
 * @pw_element int $posFinal // int
 * @pw_element string $initialDate // string
 * @pw_element string $finalDate // string
 * @pw_complex findBatches
 */
class findBatches{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $posInitial;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $posFinal;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $initialDate;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $finalDate;
}

/**
 * The complex type name definition
 *
 * @pw_element batchPojo $return // batchPojo
 * @pw_complex findBatchesResponse
 */
class findBatchesResponse{
	/**
	 * // batchPojo
	 *
	 * @var batchPojo
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_complex findAllServices
 */
class findAllServices{
}

/**
 * The complex type name definition
 *
 * @pw_element serviceEntity $return // serviceEntity
 * @pw_complex findAllServicesResponse
 */
class findAllServicesResponse{
	/**
	 * // serviceEntity
	 *
	 * @var serviceEntity
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $posInitial // int
 * @pw_element int $posFinal // int
 * @pw_complex findRangeOfServices
 */
class findRangeOfServices{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $posInitial;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $posFinal;
}

/**
 * The complex type name definition
 *
 * @pw_element serviceEntity $return // serviceEntity
 * @pw_complex findRangeOfServicesResponse
 */
class findRangeOfServicesResponse{
	/**
	 * // serviceEntity
	 *
	 * @var serviceEntity
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $id // int
 * @pw_complex findBatch
 */
class findBatch{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
}

/**
 * The complex type name definition
 *
 * @pw_element batchPojo $return // batchPojo
 * @pw_complex findBatchResponse
 */
class findBatchResponse{
	/**
	 * // batchPojo
	 *
	 * @var batchPojo
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $username // string
 * @pw_element string $password // string
 * @pw_element string $channel // string
 * @pw_element string $address // string
 * @pw_element string $timeToWait // string
 * @pw_element int $alertContent // int
 * @pw_element int $maxRepetition // int
 * @pw_element string $responseRegex // string
 * @pw_element string $messageType // string
 * @pw_element string $messageSubject // string
 * @pw_element string $messageContent // string
 * @pw_element int $idService // int
 * @pw_element boolean $interactive // boolean
 * @pw_complex createPersonalAlert
 */
class createPersonalAlert{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $username;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $password;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $channel;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $address;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $timeToWait;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $alertContent;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $maxRepetition;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $responseRegex;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageType;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageSubject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageContent;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $idService;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $interactive;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex createPersonalAlertResponse
 */
class createPersonalAlertResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $alertInstanceCode // string
 * @pw_complex removeScheduling
 */
class removeScheduling{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertInstanceCode;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex removeSchedulingResponse
 */
class removeSchedulingResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $username // string
 * @pw_element string $password // string
 * @pw_complex removePersonalAlert
 */
class removePersonalAlert{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $username;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $password;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex removePersonalAlertResponse
 */
class removePersonalAlertResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $alertInstanceCode // string
 * @pw_element string $schedulingDateTime // string
 * @pw_element string $messageType // string
 * @pw_element string $messageSubject // string
 * @pw_element string $messageContent // string
 * @pw_element int $idAlert // int
 * @pw_element replacement $replacements // replacement
 * @pw_complex editScheduling
 */
class editScheduling{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertInstanceCode;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $schedulingDateTime;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageType;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageSubject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageContent;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $idAlert;
	/**
	 * // replacement
	 *
	 * @var replacement
	 */
	public $replacements;
}

/**
 * The complex type name definition
 *
 * @pw_element string $key // string
 * @pw_element string $value // string
 * @pw_complex replacement
 */
class replacement{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $key;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $value;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex editSchedulingResponse
 */
class editSchedulingResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $runDescription // string
 * @pw_complex generateRunCode
 */
class generateRunCode{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $runDescription;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex generateRunCodeResponse
 */
class generateRunCodeResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $id // int
 * @pw_complex discartBatch
 */
class discartBatch{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
}

/**
 * The complex type name definition
 *
 * @pw_element boolean $return // boolean
 * @pw_complex discartBatchResponse
 */
class discartBatchResponse{
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $runCode // string
 * @pw_complex runScheduled
 */
class runScheduled{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $runCode;
}

/**
 * The complex type name definition
 *
 * @pw_complex runScheduledResponse
 */
class runScheduledResponse{
}

/**
 * The complex type name definition
 *
 * @pw_element int $id // int
 * @pw_complex findServiceById
 */
class findServiceById{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
}

/**
 * The complex type name definition
 *
 * @pw_element serviceEntity $return // serviceEntity
 * @pw_complex findServiceByIdResponse
 */
class findServiceByIdResponse{
	/**
	 * // serviceEntity
	 *
	 * @var serviceEntity
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $posInitial // int
 * @pw_element int $posFinal // int
 * @pw_element string $initialDate // string
 * @pw_element string $finalDate // string
 * @pw_element string $status // string
 * @pw_complex findUploadedFiles
 */
class findUploadedFiles{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $posInitial;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $posFinal;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $initialDate;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $finalDate;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $status;
}

/**
 * The complex type name definition
 *
 * @pw_element filePojo $return // filePojo
 * @pw_complex findUploadedFilesResponse
 */
class findUploadedFilesResponse{
	/**
	 * // filePojo
	 *
	 * @var filePojo
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $comment // string
 * @pw_element dateTime $createDate // dateTime
 * @pw_element string $createUser // string
 * @pw_element string $description // string
 * @pw_element string $enconding // string
 * @pw_element string $fileNameError // string
 * @pw_element string $fileNameInput // string
 * @pw_element string $fileNameOutput // string
 * @pw_element string $fileType // string
 * @pw_element int $id // int
 * @pw_element dateTime $lastUpdateDate // dateTime
 * @pw_element string $lastUpdateUser // string
 * @pw_element int $numberOfAddresses // int
 * @pw_element int $numberOfDefectiveRows // int
 * @pw_element int $numberOfEmails // int
 * @pw_element int $numberOfEmailsWithProblem // int
 * @pw_element int $numberOfPhones // int
 * @pw_element int $numberOfPhonesWithProblem // int
 * @pw_element int $numberOfRows // int
 * @pw_element int $numberOfSkippedRows // int
 * @pw_element dateTime $postingDate // dateTime
 * @pw_element long $processingTimeMsec // long
 * @pw_element string $status // string
 * @pw_element string $subject // string
 * @pw_element boolean $success // boolean
 * @pw_complex filePojo
 */
class filePojo{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $comment;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $createDate;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $createUser;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $description;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $enconding;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $fileNameError;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $fileNameInput;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $fileNameOutput;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $fileType;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $lastUpdateDate;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $lastUpdateUser;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $numberOfAddresses;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $numberOfDefectiveRows;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $numberOfEmails;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $numberOfEmailsWithProblem;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $numberOfPhones;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $numberOfPhonesWithProblem;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $numberOfRows;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $numberOfSkippedRows;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $postingDate;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $processingTimeMsec;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $status;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $subject;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $success;
}

/**
 * The complex type name definition
 *
 * @pw_element string $messageCode // string
 * @pw_complex messageWebReturn
 */
class messageWebReturn{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageCode;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex messageWebReturnResponse
 */
class messageWebReturnResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $alertInstanceCode // string
 * @pw_complex cancelAlertInstance
 */
class cancelAlertInstance{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertInstanceCode;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex cancelAlertInstanceResponse
 */
class cancelAlertInstanceResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $schedulingDateTime // string
 * @pw_element string $messageType // string
 * @pw_element string $messageSubject // string
 * @pw_element string $messageContent // string
 * @pw_element int $idAlert // int
 * @pw_element replacement $replacements // replacement
 * @pw_element string $runCode // string
 * @pw_complex newScheduling
 */
class newScheduling{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $schedulingDateTime;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageType;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageSubject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageContent;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $idAlert;
	/**
	 * // replacement
	 *
	 * @var replacement
	 */
	public $replacements;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $runCode;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex newSchedulingResponse
 */
class newSchedulingResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $idAlert // int
 * @pw_element string $messageType // string
 * @pw_element string $messageSubject // string
 * @pw_element string $messageContent // string
 * @pw_element replacement $replacements // replacement
 * @pw_element string $runCode // string
 * @pw_complex runFlow
 */
class runFlow{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $idAlert;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageType;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageSubject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageContent;
	/**
	 * // replacement
	 *
	 * @var replacement
	 */
	public $replacements;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $runCode;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex runFlowResponse
 */
class runFlowResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $id // int
 * @pw_complex findUploadedFile
 */
class findUploadedFile{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
}

/**
 * The complex type name definition
 *
 * @pw_element filePojo $return // filePojo
 * @pw_complex findUploadedFileResponse
 */
class findUploadedFileResponse{
	/**
	 * // filePojo
	 *
	 * @var filePojo
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $id // int
 * @pw_element int $phase // int
 * @pw_complex processBatch
 */
class processBatch{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $phase;
}

/**
 * The complex type name definition
 *
 * @pw_element boolean $return // boolean
 * @pw_complex processBatchResponse
 */
class processBatchResponse{
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $initialDate // string
 * @pw_element string $finalDate // string
 * @pw_element string $status // string
 * @pw_complex countUploadedFiles
 */
class countUploadedFiles{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $initialDate;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $finalDate;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $status;
}

/**
 * The complex type name definition
 *
 * @pw_element long $return // long
 * @pw_complex countUploadedFilesResponse
 */
class countUploadedFilesResponse{
	/**
	 * // long
	 *
	 * @var long
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $id // int
 * @pw_complex discartUploadedFile
 */
class discartUploadedFile{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
}

/**
 * The complex type name definition
 *
 * @pw_element boolean $return // boolean
 * @pw_complex discartUploadedFileResponse
 */
class discartUploadedFileResponse{
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $clientname // string
 * @pw_element string $username // string
 * @pw_element string $password // string
 * @pw_element string $name // string
 * @pw_element string $pinnumber // string
 * @pw_element string $receptorgroupid // string
 * @pw_complex registerReceptor
 */
class registerReceptor{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $clientname;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $username;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $password;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $name;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $pinnumber;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $receptorgroupid;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex registerReceptorResponse
 */
class registerReceptorResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $username // string
 * @pw_element string $password // string
 * @pw_element int $alertContent // int
 * @pw_element int $maxRepetition // int
 * @pw_element string $messageType // string
 * @pw_element string $messageSubject // string
 * @pw_element string $messageContent // string
 * @pw_element personalAlertStep $personalAlertStep // personalAlertStep
 * @pw_element int $idService // int
 * @pw_element boolean $interactive // boolean
 * @pw_complex createPersonalAlertPlus
 */
class createPersonalAlertPlus{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $username;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $password;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $alertContent;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $maxRepetition;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageType;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageSubject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageContent;
	/**
	 * // personalAlertStep
	 *
	 * @var personalAlertStep
	 */
	public $personalAlertStep;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $idService;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $interactive;
}

/**
 * The complex type name definition
 *
 * @pw_element string $address // string
 * @pw_element string $channel // string
 * @pw_element int $id // int
 * @pw_element personalAlertConditional $personalAlertConditional // personalAlertConditional
 * @pw_element string $stepContent // string
 * @pw_element string $stepSubject // string
 * @pw_element string $stepType // string
 * @pw_element string $timeToWait // string
 * @pw_complex personalAlertStep
 */
class personalAlertStep{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $address;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $channel;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // personalAlertConditional
	 *
	 * @var personalAlertConditional
	 */
	public $personalAlertConditional;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $stepContent;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $stepSubject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $stepType;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $timeToWait;
}

/**
 * The complex type name definition
 *
 * @pw_element int $nextPersonalAlertStepId // int
 * @pw_element string $responseRegex // string
 * @pw_element boolean $stopIfSuccess // boolean
 * @pw_element boolean $timeoutOccured // boolean
 * @pw_complex personalAlertConditional
 */
class personalAlertConditional{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $nextPersonalAlertStepId;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $responseRegex;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $stopIfSuccess;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $timeoutOccured;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex createPersonalAlertPlusResponse
 */
class createPersonalAlertPlusResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $hour // string
 * @pw_element string $minute // string
 * @pw_element string $second // string
 * @pw_complex setTimerFileUpload
 */
class setTimerFileUpload{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $hour;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $minute;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $second;
}

/**
 * The complex type name definition
 *
 * @pw_complex setTimerFileUploadResponse
 */
class setTimerFileUploadResponse{
}

/**
 * The complex type name definition
 *
 * @pw_element string $schedulingDateTime // string
 * @pw_element string $messageType // string
 * @pw_element string $messageSubject // string
 * @pw_element string $messageContent // string
 * @pw_element int $receptorgroupid // int
 * @pw_element replacement $replacements // replacement
 * @pw_element string $runCode // string
 * @pw_complex schedulePersonalAlertByReceptorGroup
 */
class schedulePersonalAlertByReceptorGroup{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $schedulingDateTime;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageType;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageSubject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageContent;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $receptorgroupid;
	/**
	 * // replacement
	 *
	 * @var replacement
	 */
	public $replacements;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $runCode;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex schedulePersonalAlertByReceptorGroupResponse
 */
class schedulePersonalAlertByReceptorGroupResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $idReceptorGroup // int
 * @pw_element string $receptorGroupName // string
 * @pw_complex editReceptorGroup
 */
class editReceptorGroup{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $idReceptorGroup;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $receptorGroupName;
}

/**
 * The complex type name definition
 *
 * @pw_element receptorGroupEntity $return // receptorGroupEntity
 * @pw_complex editReceptorGroupResponse
 */
class editReceptorGroupResponse{
	/**
	 * // receptorGroupEntity
	 *
	 * @var receptorGroupEntity
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $runCode // string
 * @pw_complex cancelAlertInstanceByRunCode
 */
class cancelAlertInstanceByRunCode{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $runCode;
}

/**
 * The complex type name definition
 *
 * @pw_element int $return // int
 * @pw_complex cancelAlertInstanceByRunCodeResponse
 */
class cancelAlertInstanceByRunCodeResponse{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $idReceptorGroup // int
 * @pw_complex getReceptorGroupById
 */
class getReceptorGroupById{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $idReceptorGroup;
}

/**
 * The complex type name definition
 *
 * @pw_element receptorGroupEntity $return // receptorGroupEntity
 * @pw_complex getReceptorGroupByIdResponse
 */
class getReceptorGroupByIdResponse{
	/**
	 * // receptorGroupEntity
	 *
	 * @var receptorGroupEntity
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $alertInstanceCode // string
 * @pw_complex getRSVPByAlertInstanceCode
 */
class getRSVPByAlertInstanceCode{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertInstanceCode;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex getRSVPByAlertInstanceCodeResponse
 */
class getRSVPByAlertInstanceCodeResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $username // string
 * @pw_element string $password // string
 * @pw_complex getPersonalAlert
 */
class getPersonalAlert{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $username;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $password;
}

/**
 * The complex type name definition
 *
 * @pw_element personalAlertReturn $return // personalAlertReturn
 * @pw_complex getPersonalAlertResponse
 */
class getPersonalAlertResponse{
	/**
	 * // personalAlertReturn
	 *
	 * @var personalAlertReturn
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element alertEntity $alert // alertEntity
 * @pw_element alertStepEntity $alertStepsCollection // alertStepEntity
 * @pw_element receptorEntity $receptor // receptorEntity
 * @pw_complex personalAlertReturn
 */
class personalAlertReturn{
	/**
	 * // alertEntity
	 *
	 * @var alertEntity
	 */
	public $alert;
	/**
	 * // alertStepEntity
	 *
	 * @var alertStepEntity
	 */
	public $alertStepsCollection;
	/**
	 * // receptorEntity
	 *
	 * @var receptorEntity
	 */
	public $receptor;
}

/**
 * The complex type name definition
 *
 * @pw_element alertContentEntity $alertContent // alertContentEntity
 * @pw_element string $alertDescription // string
 * @pw_element string $alertName // string
 * @pw_element formatGroupEntity $formatGroup // formatGroupEntity
 * @pw_element int $id // int
 * @pw_element boolean $interactive // boolean
 * @pw_element int $maxRepetition // int
 * @pw_element receptorEntity $receptor // receptorEntity
 * @pw_element serviceEntity $service // serviceEntity
 * @pw_element long $version // long
 * @pw_complex alertEntity
 */
class alertEntity{
	/**
	 * // alertContentEntity
	 *
	 * @var alertContentEntity
	 */
	public $alertContent;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertDescription;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertName;
	/**
	 * // formatGroupEntity
	 *
	 * @var formatGroupEntity
	 */
	public $formatGroup;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $interactive;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $maxRepetition;
	/**
	 * // receptorEntity
	 *
	 * @var receptorEntity
	 */
	public $receptor;
	/**
	 * // serviceEntity
	 *
	 * @var serviceEntity
	 */
	public $service;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element string $alertContent // string
 * @pw_element string $alertSubject // string
 * @pw_element string $alertType // string
 * @pw_element int $id // int
 * @pw_element long $version // long
 * @pw_complex alertContentEntity
 */
class alertContentEntity{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertContent;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertSubject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertType;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element dateTime $createDate // dateTime
 * @pw_element scUserEntity $createUser // scUserEntity
 * @pw_element string $formatGroupDescription // string
 * @pw_element string $formatGroupName // string
 * @pw_element long $id // long
 * @pw_element dateTime $lastUpdateDate // dateTime
 * @pw_element scUserEntity $lastUpdateUser // scUserEntity
 * @pw_element long $version // long
 * @pw_complex formatGroupEntity
 */
class formatGroupEntity{
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $createDate;
	/**
	 * // scUserEntity
	 *
	 * @var scUserEntity
	 */
	public $createUser;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $formatGroupDescription;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $formatGroupName;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $id;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $lastUpdateDate;
	/**
	 * // scUserEntity
	 *
	 * @var scUserEntity
	 */
	public $lastUpdateUser;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element alertEntity $alert // alertEntity
 * @pw_element int $id // int
 * @pw_element string $login // string
 * @pw_element string $name // string
 * @pw_element string $password // string
 * @pw_element long $version // long
 * @pw_complex scUserEntity
 */
class scUserEntity{
	/**
	 * // alertEntity
	 *
	 * @var alertEntity
	 */
	public $alert;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $login;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $name;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $password;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element alertEntity $alertDefault // alertEntity
 * @pw_element int $id // int
 * @pw_element string $pinnumber // string
 * @pw_element string $receptorDescription // string
 * @pw_element receptorGroupEntity $receptorGroup // receptorGroupEntity
 * @pw_element string $receptorName // string
 * @pw_element string $receptorPassword // string
 * @pw_element string $receptorType // string
 * @pw_element string $receptorUserName // string
 * @pw_element long $version // long
 * @pw_complex receptorEntity
 */
class receptorEntity{
	/**
	 * // alertEntity
	 *
	 * @var alertEntity
	 */
	public $alertDefault;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $pinnumber;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $receptorDescription;
	/**
	 * // receptorGroupEntity
	 *
	 * @var receptorGroupEntity
	 */
	public $receptorGroup;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $receptorName;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $receptorPassword;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $receptorType;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $receptorUserName;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element string $alertStepName // string
 * @pw_element long $alertStepWaitTime // long
 * @pw_element channelReceptorEntity $channelReceptor // channelReceptorEntity
 * @pw_element int $id // int
 * @pw_element string $messageContent // string
 * @pw_element string $messageSubject // string
 * @pw_element string $messageType // string
 * @pw_element long $version // long
 * @pw_complex alertStepEntity
 */
class alertStepEntity{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $alertStepName;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $alertStepWaitTime;
	/**
	 * // channelReceptorEntity
	 *
	 * @var channelReceptorEntity
	 */
	public $channelReceptor;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageContent;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageSubject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageType;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element string $addresses // string
 * @pw_element channelEntity $channel // channelEntity
 * @pw_element int $id // int
 * @pw_element receptorEntity $receptor // receptorEntity
 * @pw_element long $version // long
 * @pw_complex channelReceptorEntity
 */
class channelReceptorEntity{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $addresses;
	/**
	 * // channelEntity
	 *
	 * @var channelEntity
	 */
	public $channel;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // receptorEntity
	 *
	 * @var receptorEntity
	 */
	public $receptor;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element billingCategoryEntity $billingCategory // billingCategoryEntity
 * @pw_element string $channelDescription // string
 * @pw_element string $channelName // string
 * @pw_element int $idchannel // int
 * @pw_element long $version // long
 * @pw_complex channelEntity
 */
class channelEntity{
	/**
	 * // billingCategoryEntity
	 *
	 * @var billingCategoryEntity
	 */
	public $billingCategory;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $channelDescription;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $channelName;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $idchannel;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element string $billingCategoryDescription // string
 * @pw_element string $billingCategoryName // string
 * @pw_element string $billingCategoryRule // string
 * @pw_element billingTransactionTypeEntity $billingCategoryTransactionType // billingTransactionTypeEntity
 * @pw_element billingTransactionTypeEntity $billingCategoryTransactionTypeConfirm // billingTransactionTypeEntity
 * @pw_element billingTransactionTypeEntity $billingCategoryTransactionTypeReturn // billingTransactionTypeEntity
 * @pw_element dateTime $createDate // dateTime
 * @pw_element $createUser; $/ scUserEntity
 * @pw_element long $id // long
 * @pw_element dateTime $lastUpdateDate // dateTime
 * @pw_element $lastUpdateUser; $/ scUserEntity
 * @pw_element long $version // long
 * @pw_complex billingCategoryEntity
 */
class billingCategoryEntity{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $billingCategoryDescription;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $billingCategoryName;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $billingCategoryRule;
	/**
	 * // billingTransactionTypeEntity
	 *
	 * @var billingTransactionTypeEntity
	 */
	public $billingCategoryTransactionType;
	/**
	 * // billingTransactionTypeEntity
	 *
	 * @var billingTransactionTypeEntity
	 */
	public $billingCategoryTransactionTypeConfirm;
	/**
	 * // billingTransactionTypeEntity
	 *
	 * @var billingTransactionTypeEntity
	 */
	public $billingCategoryTransactionTypeReturn;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $createDate;
	/**
	 * scUserEntity
	 *
	 * @var $createUser;
	 */
	public $/;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $id;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $lastUpdateDate;
	/**
	 * scUserEntity
	 *
	 * @var $lastUpdateUser;
	 */
	public $/;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element string $billingTransactionCommittedInventory // string
 * @pw_element string $billingTransactionContractedInventory // string
 * @pw_element string $billingTransactionRegister // string
 * @pw_element string $billingTransactionTypeDescription // string
 * @pw_element string $billingTransactionTypeName // string
 * @pw_element string $billingTransactionUsedInventory // string
 * @pw_element dateTime $createDate // dateTime
 * @pw_element scUserEntity $createUser // scUserEntity
 * @pw_element long $id // long
 * @pw_element dateTime $lastUpdateDate // dateTime
 * @pw_element scUserEntity $lastUpdateUser // scUserEntity
 * @pw_element long $version // long
 * @pw_complex billingTransactionTypeEntity
 */
class billingTransactionTypeEntity{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $billingTransactionCommittedInventory;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $billingTransactionContractedInventory;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $billingTransactionRegister;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $billingTransactionTypeDescription;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $billingTransactionTypeName;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $billingTransactionUsedInventory;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $createDate;
	/**
	 * // scUserEntity
	 *
	 * @var scUserEntity
	 */
	public $createUser;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $id;
	/**
	 * // dateTime
	 *
	 * @var dateTime
	 */
	public $lastUpdateDate;
	/**
	 * // scUserEntity
	 *
	 * @var scUserEntity
	 */
	public $lastUpdateUser;
	/**
	 * // long
	 *
	 * @var long
	 */
	public $version;
}

/**
 * The complex type name definition
 *
 * @pw_element string $name // string
 * @pw_element string $description // string
 * @pw_element string $origin // string
 * @pw_element string $workdays // string
 * @pw_element string $workingHours // string
 * @pw_element boolean $delayOfOneDayInTheFirstStep // boolean
 * @pw_element string $callBackUrl // string
 * @pw_element string $callBackUser // string
 * @pw_element string $callBackPassword // string
 * @pw_complex createService
 */
class createService{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $name;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $description;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $origin;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $workdays;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $workingHours;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $delayOfOneDayInTheFirstStep;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $callBackUrl;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $callBackUser;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $callBackPassword;
}

/**
 * // Service id froM created Service
 *
 * @pw_element int $return // int
 * @pw_complex createServiceResponse
 */
class createServiceResponse{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $messageCode // string
 * @pw_element string $messageResponse // string
 * @pw_complex messageWebResponse
 */
class messageWebResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageCode;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $messageResponse;
}

/**
 * The complex type name definition
 *
 * @pw_element string $return // string
 * @pw_complex messageWebResponseResponse
 */
class messageWebResponseResponse{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $id // int
 * @pw_element int $idEmpresa // int
 * @pw_complex importUploadedFile
 */
class importUploadedFile{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $idEmpresa;
}

/**
 * The complex type name definition
 *
 * @pw_element boolean $return // boolean
 * @pw_complex importUploadedFileResponse
 */
class importUploadedFileResponse{
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $runCode // string
 * @pw_complex runExecuted
 */
class runExecuted{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $runCode;
}

/**
 * The complex type name definition
 *
 * @pw_element string $subject // string
 * @pw_element string $description // string
 * @pw_element string $schedulingDateTime // string
 * @pw_element int $filterId // int
 * @pw_element int $serviceId // int
 * @pw_element string $sqlQuery // string
 * @pw_element boolean $interactive // boolean
 * @pw_complex createBatch
 */
class createBatch{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $subject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $description;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $schedulingDateTime;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $filterId;
	/**
	 * // int
	 *
	 * @var int
	 */
	public $serviceId;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $sqlQuery;
	/**
	 * // boolean
	 *
	 * @var boolean
	 */
	public $interactive;
}

/**
 * The complex type name definition
 *
 * @pw_element batchPojo $return // batchPojo
 * @pw_complex createBatchResponse
 */
class createBatchResponse{
	/**
	 * // batchPojo
	 *
	 * @var batchPojo
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element string $initialDate // string
 * @pw_element string $finalDate // string
 * @pw_complex countBatches
 */
class countBatches{
	/**
	 * // string
	 *
	 * @var string
	 */
	public $initialDate;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $finalDate;
}

/**
 * The complex type name definition
 *
 * @pw_element long $return // long
 * @pw_complex countBatchesResponse
 */
class countBatchesResponse{
	/**
	 * // long
	 *
	 * @var long
	 */
	public $return;
}

/**
 * The complex type name definition
 *
 * @pw_element int $id // int
 * @pw_element string $subject // string
 * @pw_element string $description // string
 * @pw_complex updateUploadedFile
 */
class updateUploadedFile{
	/**
	 * // int
	 *
	 * @var int
	 */
	public $id;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $subject;
	/**
	 * // string
	 *
	 * @var string
	 */
	public $description;
}

/**
 * The complex type name definition
 *
 * @pw_element filePojo $return // filePojo
 * @pw_complex updateUploadedFileResponse
 */
class updateUploadedFileResponse{
	/**
	 * // filePojo
	 *
	 * @var filePojo
	 */
	public $return;
}
